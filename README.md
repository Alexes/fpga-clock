## Project description
This project is an `HH:MM:SS` adjustable clock built around an FPGA. See the bottom of the description for a photo of the device.

The project was moved to Bitbucket in August of 2018. The project was done in Junary of 2015 and was originally published as a [blogpost](https://burubaxair.wordpress.com/2015/01/07/a-simple-digital-clock-implemented-in-an-fpga/) by Dr. Vladislav Gladkikh.

## Project members
* Dr. Vladislav Gladkikh -- idea, guidance, provision of the hardware and tools.
* Aktolkyn Oteuova -- soldering and assembly.
* Alexandr Nigay -- Verilog coding and the design of the external circuits.

## Technical info
The project was developed and run on an Altera Cyclone IV (EP4CE6E22C8N) FPGA mounted on an [EP4CE6 development board](https://www.waveshare.com/product/fpga-tools/altera/openep4ce6-c-standard.htm) from Waveshare.

The Verilog source of the project comprises three files:

* `SevenSegment_HHMMSS_WithButtons.v` -- contains the main Verilog module of the project. It is responsible for the time-keeping logic and for the adjustment logic.

* `PushButtonDebouncer.v` -- the "debouncer" used for the buttons. The purpose of a debouncer is to smooth out the button presses&mdash;without such filtering, a single button press, as perceived by a human operator, is actually detected by digital electronics as multiple presses. More information is available [here](https://www.fpga4fun.com/Debouncer.html).

* `BCDto7Seg.v` -- for each binary-coded decimal digit, it determines how to correctly light up a 7-segment display. 

The circuit diagrams in `Clock-HHMMSS-CircuitDiagram.pdf` describe how the buttons and the 7-segment displays connect to the FPGA development board.

## Photo of the device

Assembled and powered-on device looks like this:

![Photo of the clock](https://bitbucket.org/Alexes/fpga-clock/raw/master/FPGA-clock-working.jpg =250x)
