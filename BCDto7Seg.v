// BCD to common-anode 7-segment display converter
module BCDto7Seg (
  input [3:0] BCD,
  output reg [7:0] SevenSegment
);

  // for common-anode display, inverted signals are required
  always @ (*) // shorthand for always @ (BCD)
    case (BCD)
      4'h0: SevenSegment = ~8'b11111100;
      4'h1: SevenSegment = ~8'b01100000;
      4'h2: SevenSegment = ~8'b11011010;
      4'h3: SevenSegment = ~8'b11110010;
      4'h4: SevenSegment = ~8'b01100110;
      4'h5: SevenSegment = ~8'b10110110;
      4'h6: SevenSegment = ~8'b10111110;
      4'h7: SevenSegment = ~8'b11100000;
      4'h8: SevenSegment = ~8'b11111110;
      4'h9: SevenSegment = ~8'b11110110;
      default: SevenSegment = ~8'b00000000;
    endcase

endmodule