module SevenSegment_HHMMSS_WithButtons (
  input clk, 
  input [5:0] buttons, // 6 active-low push buttons, one for each digit
  output reg [7:0] LED7Seg_Cathodes, // Cathodes[7] = A, Cathodes[1] = G, Cathodes[0] = DP
  output reg [5:0] LED7Seg_Anodes // MSB = tens of hours display, LSB = units of seconds display
);

  // 50MHz to 100Hz converter
  wire clk_mux;
  FrequencyDivider_Mux dividerMux(.clk_in(clk), .clk_out(clk_mux));

  // registers to hold time values
  reg [3:0] seconds0_reg = 4'd0;
  reg [3:0] seconds1_reg = 4'd0;
  reg [3:0] minutes0_reg = 4'd0;
  reg [3:0] minutes1_reg = 4'd0;
  reg [3:0] hours0_reg = 4'd0;
  reg [3:0] hours1_reg = 4'd0;
  
  //
  // adjustment push buttons
  //
  wire [5:0] button_states; // actually, not used
  wire [5:0] button_down; // a one clock period HIGH pulse when a particular button is pressed
  wire any_button_down = |button_down; // OR of all bits
  PushButtonDebouncer button_ss0_debouncer(.clk(clk), .button_raw(buttons[0]), .button_state_clean(button_states[0]), .button_down(button_down[0]));
  PushButtonDebouncer button_ss1_debouncer(.clk(clk), .button_raw(buttons[1]), .button_state_clean(button_states[1]), .button_down(button_down[1]));
  PushButtonDebouncer button_mm0_debouncer(.clk(clk), .button_raw(buttons[2]), .button_state_clean(button_states[2]), .button_down(button_down[2]));
  PushButtonDebouncer button_mm1_debouncer(.clk(clk), .button_raw(buttons[3]), .button_state_clean(button_states[3]), .button_down(button_down[3]));
  PushButtonDebouncer button_hh0_debouncer(.clk(clk), .button_raw(buttons[4]), .button_state_clean(button_states[4]), .button_down(button_down[4]));
  PushButtonDebouncer button_hh1_debouncer(.clk(clk), .button_raw(buttons[5]), .button_state_clean(button_states[5]), .button_down(button_down[5]));
  
  //
  // 1Hz generator
  //
  reg [26:0] clk_1Hz_cnt = 27'b0;  
  wire clk_1Hz_reached = (clk_1Hz_cnt == 27'b0); // HIGH for one clock period once per second
  
  always @ (posedge clk)
    if (any_button_down)
      clk_1Hz_cnt <= 27'd100_000_000; // a 2 second pause
    else
      clk_1Hz_cnt <= (clk_1Hz_reached) ? 27'd50_000_000 : (clk_1Hz_cnt - 27'b1);
  
  // 
  // incrementing the time by 1 second
  // 
  wire ss0_reached_9 = seconds0_reg == 4'd9;
  wire ss1_reached_5 = seconds1_reg == 4'd5;
  wire mm0_reached_9 = minutes0_reg == 4'd9;
  wire mm1_reached_5 = minutes1_reg == 4'd5;
  wire hh0_reached_9 = hours0_reg == 4'd9;
  wire hh0_reached_3 = hours0_reg == 4'd3;
  wire hh1_reached_2 = hours1_reg == 4'd2;
  
  wire ss0_should_increment = clk_1Hz_reached;
  wire ss1_should_increment = ss0_should_increment & ss0_reached_9;
  wire mm0_should_increment = ss1_should_increment & ss1_reached_5;    
  wire mm1_should_increment = mm0_should_increment & mm0_reached_9;
  wire hh0_should_increment = mm1_should_increment & mm1_reached_5;
  wire hh1_should_increment = (hh0_should_increment & ~hh1_reached_2 & hh0_reached_9) | 
                              (hh0_should_increment &  hh1_reached_2 & hh0_reached_3);
  
  always @ (posedge clk)
    if (ss0_should_increment | button_down[0])
      seconds0_reg <= ss0_reached_9 ? 4'b0 : seconds0_reg + 4'b1;
  
  always @ (posedge clk)
    if (ss1_should_increment | button_down[1]) 
      seconds1_reg <= ss1_reached_5 ? 4'b0 : seconds1_reg + 4'b1;
  
  always @ (posedge clk)
    if (mm0_should_increment | button_down[2])
      minutes0_reg <= mm0_reached_9 ? 4'b0 : minutes0_reg + 4'b1;
  
  always @ (posedge clk)
    if (mm1_should_increment | button_down[3])
      minutes1_reg <= mm1_reached_5 ? 4'b0 : minutes1_reg + 4'b1;
  
  always @ (posedge clk)
    if (hh0_should_increment | button_down[4]) begin
      if (hh1_reached_2)
        hours0_reg <= hh0_reached_3 ? 4'b0 : hours0_reg + 4'b1;
      else
        hours0_reg <= hh0_reached_9 ? 4'b0 : hours0_reg + 4'b1;
    end
  
  always @ (posedge clk)
    if (hh1_should_increment | button_down[5]) begin
      if (button_down[5]) begin // Button is pressed, logic is different.
        if (hours0_reg < 4'd4)  // Hours1 allowed to cycle from 0 to 2 if Hours0 is less than or equal to 3
          hours1_reg <= hh1_reached_2 ? 4'b0 : hours1_reg + 4'b1;
        else                    // Hours1 allowed to cycle from 0 to 1 only if Hours0 is greater than 3
          hours1_reg <= (hours1_reg == 4'b1) ? 4'b0 : hours1_reg + 4'b1;
      end 
      else  // Button is not pressed, increment because of time passage.
        hours1_reg <= hh1_reached_2 ? 4'b0 : hours1_reg + 4'b1;
    end
  
  
  
  // connecting registers to BCD-to-7Segment decoders
  wire [7:0] ss0_wires;
  wire [7:0] ss1_wires;
  wire [7:0] mm0_wires;
  wire [7:0] mm1_wires;
  wire [7:0] hh0_wires;
  wire [7:0] hh1_wires;
  BCDto7Seg ss0_decoder(.BCD(seconds0_reg), .SevenSegment(ss0_wires));
  BCDto7Seg ss1_decoder(.BCD(seconds1_reg), .SevenSegment(ss1_wires));
  BCDto7Seg mm0_decoder(.BCD(minutes0_reg), .SevenSegment(mm0_wires));
  BCDto7Seg mm1_decoder(.BCD(minutes1_reg), .SevenSegment(mm1_wires));
  BCDto7Seg hh0_decoder(.BCD(hours0_reg), .SevenSegment(hh0_wires));
  BCDto7Seg hh1_decoder(.BCD(hours1_reg), .SevenSegment(hh1_wires));

  // multiplexing
  reg [2:0] current_digit = 3'b0;
  always @ (posedge clk_mux)
  begin
    current_digit <= (current_digit == 3'd5) ? 3'b0 : current_digit + 3'b1;
    case (current_digit)
      3'd0: begin
        LED7Seg_Cathodes <= ss0_wires;
        LED7Seg_Anodes <= 6'b000001;
      end
      3'd1: begin
        LED7Seg_Cathodes <= ss1_wires;
        LED7Seg_Anodes <= 6'b000010;
      end
      3'd2: begin
        LED7Seg_Cathodes <= mm0_wires;
        LED7Seg_Anodes <= 6'b000100;
      end
      3'd3: begin
        LED7Seg_Cathodes <= mm1_wires;
        LED7Seg_Anodes <= 6'b001000;
      end
      3'd4: begin
        LED7Seg_Cathodes <= hh0_wires;
        LED7Seg_Anodes <= 6'b010000;
      end
      3'd5: begin
        LED7Seg_Cathodes <= hh1_wires;
        LED7Seg_Anodes <= 6'b100000;
      end
    endcase
  end
  
  
endmodule



// 50MHz clock to 1200Hz 50% duty cycle
module FrequencyDivider_Mux (
  input clk_in,
  output reg clk_out = 1'b1
);

  reg [17:0] clk_out_cnt = 18'b0;
  always @ (posedge clk_in)
  begin
    if (clk_out_cnt == 18'd21000) // 
    begin
      clk_out_cnt <= 18'b0;
      clk_out <= ~clk_out;
    end
    else
      clk_out_cnt <= clk_out_cnt + 1'b1;
  end

endmodule


