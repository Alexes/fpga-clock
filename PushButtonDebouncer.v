module PushButtonDebouncer(
  input clk,
  input button_raw, // input bouncing active-low push button signal
  output reg button_state_clean = 1'b0, // output clean active-high push button signal
  output button_down // generates a pulse when the button is pressed
);

  // Sync button signal to clk domain
  reg button_sync_0;
  reg button_sync_1;
  always @ (posedge clk) button_sync_0 <= ~button_raw; // negate to make active-high
  always @ (posedge clk) button_sync_1 <= button_sync_0;
    
  // When this counter overflows, we believe that button state changed.
  // This is equivalent to waiting for 1/(5e7/2^17) seconds of stable signal.
  reg [16:0] button_cnt = 17'b0; // 2.6ms
  
  wire button_idle = (button_state_clean == button_sync_1);
  wire button_cnt_max = &button_cnt;
  
  // debounce
  always @ (posedge clk) begin
    if (button_idle)
      button_cnt <= 17'b0;
    else begin
      button_cnt <= button_cnt + 17'b1;
      if (button_cnt_max) button_state_clean <= ~button_state_clean;
    end
  end
  
  // this signal becomes 1 for one clock cycle just prior to the change in button_state_clean from 0 to 1
  assign button_down = ~button_idle & button_cnt_max & ~button_state_clean; // on the next clk, button_state_clean will change
  
endmodule
